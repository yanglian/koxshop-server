package main

import (
	"net/http"
	"gopkg.in/gin-gonic/gin.v1"
	"koxshop-server/controller/user"
	"koxshop-server/controller/admin"
	"koxshop-server/config"
)


func initRouter() *gin.Engine {
	gin.SetMode(config.Common.Mode) //gin.DebugMode
	router := gin.Default()
	api := router.Group("/api")
	admin := router.Group("/admin")

	router.GET("/", func(c *gin.Context) {
		c.JSON(http.StatusOK, gin.H{"message": "ok"})
	})

	//客户端API路由
	api.GET("/user", user.GetUserList) 

	//管理端API路由
	admin.GET("/create", admins.Create)
    admin.GET("/login", admins.Login)
	return router
}
