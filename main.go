package main

import (
	"strconv"
	"fmt"
	"github.com/cihub/seelog"
	"koxshop-server/config"
)

func initLog() {
	logger, err := seelog.LoggerFromConfigAsFile("config/seelog.xml")
	if err != nil {
		seelog.Critical("解析配置日志文件出错", err)
		return
	}
	seelog.ReplaceLogger(logger)
	defer seelog.Flush()
}


func init() {
	initLog()
}

func main() {
	fmt.Println("database config:",config.Db)
	router := initRouter()
	router.Static("/static", "./static")
	router.Run(":" + strconv.Itoa(config.Common.Prot))
}
