package config

import (
	"io/ioutil"
	"gopkg.in/yaml.v2"
	"github.com/cihub/seelog"
)

type Config struct {
	Domain string `yaml:"domain"`
	Prot int `yaml:"prot"`
	Mode string `yaml:"mode"`
}

type Database struct {
	Type string `yaml:"type"`
	Hostname string `yaml:"hostname"`
	Database string `yaml:"database"`
	Username string `yaml:"username"`
	Password string `yaml:"password"`
	Hostport int `yaml:"hostport"`
	Prefix string `yaml:"prefix"`
	Charset string `yaml:"charset"`
	Debug bool `yaml:"debug"`
}

var Common Config
var Db Database

func initConfig()  {
	data, err := ioutil.ReadFile("config/config.yaml")
	if err != nil {
		seelog.Error(err)
	}
	err = yaml.Unmarshal(data, &Common)
	if err != nil {
		seelog.Error(err)
	}
}

func initDB()  {
	data, err := ioutil.ReadFile("config/database.yaml")
	if err != nil {
		seelog.Error(err)
	}
	err = yaml.Unmarshal(data, &Db)
	if err != nil {
		seelog.Error(err)
	}
}

func init()  {
	initDB()
	initConfig()
}